*** Life<FredkinCell> 6x10 ***

Generation = 0, Population = 19.
--00---0--
0--0-0-0-0
0-----0-0-
--0---0--0
----00----
0---0--0--

*** Life<FredkinCell> 6x7 ***

Generation = 0, Population = 16.
00----0
-000---
00---0-
-------
0--0--0
-000-0-

Generation = 19, Population = 20.
6-577-7
62-7---
-----37
1--446-
-6-----
65-7-73

*** Life<FredkinCell> 2x5 ***

Generation = 0, Population = 5.
00000
-----

Generation = 2, Population = 3.
2-0-2
-----

Generation = 4, Population = 3.
2-0-2
-----

Generation = 6, Population = 3.
2-0-2
-----

Generation = 8, Population = 3.
2-0-2
-----

Generation = 10, Population = 3.
2-0-2
-----

Generation = 12, Population = 3.
2-0-2
-----

Generation = 14, Population = 3.
2-0-2
-----

Generation = 16, Population = 3.
2-0-2
-----

*** Life<FredkinCell> 7x2 ***

Generation = 0, Population = 12.
00
00
00
00
00
0-
0-

Generation = 1, Population = 9.
--
11
11
11
1-
--
10

Generation = 2, Population = 9.
00
--
22
2-
2-
-0
21

Generation = 3, Population = 7.
11
--
-3
--
3-
01
3-

*** Life<FredkinCell> 3x2 ***

Generation = 0, Population = 3.
00
--
-0

*** Life<FredkinCell> 2x7 ***

Generation = 0, Population = 7.
0000000
-------

*** Life<FredkinCell> 4x7 ***

Generation = 0, Population = 18.
00-00-0
-0000--
-000000
00-0---

*** Life<FredkinCell> 9x6 ***

Generation = 0, Population = 21.
0--00-
-00---
----00
-0--0-
--0---
--0-00
00--0-
0--0--
0-0--0

Generation = 16, Population = 33.
611-3-
64-5-1
-02--+
4-4---
4-31-9
64-137
--314-
--2912
56-6-3

*** Life<FredkinCell> 10x6 ***

Generation = 0, Population = 22.
----0-
--0---
---000
0---00
0-00--
0-0-0-
-0-0--
0-----
---0--
-0-000

*** Life<FredkinCell> 4x9 ***

Generation = 0, Population = 17.
0-0000--0
-------00
-00---00-
--00000--

*** Life<FredkinCell> 5x8 ***

Generation = 0, Population = 19.
0--0--0-
000-----
000---00
--0--00-
0000--0-

*** Life<FredkinCell> 6x5 ***

Generation = 0, Population = 19.
0-000
-0000
00000
0---0
-0--0
--0-0

Generation = 5, Population = 15.
--210
----5
--5-2
512-1
-0-2-
22--2

Generation = 10, Population = 17.
04---
1-3--
42---
-4234
00--2
-2213

Generation = 15, Population = 13.
0---0
-----
-2834
-53-4
-5---
73-1-

Generation = 20, Population = 19.
279-0
3---5
54+4-
-7-4-
36336
8-2--

Generation = 25, Population = 11.
---3-
-5---
-7+-5
98---
--6-7
+---9

*** Life<FredkinCell> 8x4 ***

Generation = 0, Population = 15.
-0--
--00
-0--
--0-
0-00
00-0
-0-0
0-0-

Generation = 10, Population = 14.
2-0-
-2-2
2-3-
3--0
---0
----
3--2
0-24

*** Life<FredkinCell> 5x5 ***

Generation = 0, Population = 7.
----0
-0-0-
----0
----0
0--0-

Generation = 26, Population = 12.
06+--
0-0-1
660--
-----
+7+--

*** Life<FredkinCell> 5x10 ***

Generation = 0, Population = 23.
000-0-0---
0----00--0
-0-00-0--0
00---0---0
---0-0-000

*** Life<FredkinCell> 4x3 ***

Generation = 0, Population = 12.
000
000
000
000

*** Life<FredkinCell> 4x6 ***

Generation = 0, Population = 11.
00-000
-----0
0----0
0--00-

Generation = 11, Population = 13.
6-2--2
--43-2
2---1-
-35423

*** Life<FredkinCell> 9x4 ***

Generation = 0, Population = 9.
----
0-0-
----
-000
0---
0-0-
-0--
----
----

Generation = 4, Population = 15.
0---
0---
-1--
2-01
--2-
--1-
-0-2
-100
00--

Generation = 8, Population = 15.
-3--
2---
5---
4-13
513-
--2-
4--4
-321
----

*** Life<FredkinCell> 8x6 ***

Generation = 0, Population = 8.
---0--
--0---
-0----
0-----
-0----
0-----
--0---
---0--

Generation = 3, Population = 16.
--0-0-
-0----
0-0-0-
--00--
012-0-
00----
---0--
----0-

Generation = 6, Population = 25.
-01--0
0-20--
1--01-
01-1-1
12-0-0
-0-02-
-12-11
----1-

Generation = 9, Population = 18.
-32---
--3-1-
-22---
-111-2
1-----
-03-3-
---2-2
0-2---

Generation = 12, Population = 23.
2-2---
--31-2
1--34-
21223-
--2--4
036-4-
-3---2
-23---

Generation = 15, Population = 8.
----1-
------
------
-1----
1-2---
--6---
33----
----2-

Generation = 18, Population = 18.
-----0
--3---
-254-0
--443-
-7-1-4
---04-
445---
-----0

Generation = 21, Population = 24.
-5--1-
31---2
---56-
3---44
-8--05
--7--0
5-8254
-2424-

Generation = 24, Population = 24.
28-11-
-1---2
3-7-60
--55--
-8-106
-5+0--
549-6-
-----0

*** Life<FredkinCell> 3x4 ***

Generation = 0, Population = 10.
0000
0000
-0-0

*** Life<FredkinCell> 6x7 ***

Generation = 0, Population = 9.
------0
-------
---0--0
-----00
--00---
-00----

*** Life<FredkinCell> 7x6 ***

Generation = 0, Population = 21.
00-0-0
0000-0
-000--
--0--0
-0---0
---00-
--00-0

*** Life<FredkinCell> 6x6 ***

Generation = 0, Population = 5.
---0--
0-----
-0----
--0---
---0--
------

Generation = 1, Population = 6.
0-0-0-
---0--
------
------
----0-
---0--

Generation = 2, Population = 7.
---0-0
0-----
---0--
----0-
-----0
--0---

Generation = 3, Population = 10.
0-0---
-0---0
0-0---
------
--0---
-0-0-0

Generation = 4, Population = 13.
-0-0-0
0-0-0-
-0-0-0
0-----
-----0
0-0---

Generation = 5, Population = 11.
--0-0-
-----0
0-0-0-
---0--
--0-0-
---0-0

Generation = 6, Population = 9.
-0----
0---0-
---0--
0-0-0-
-0----
----0-

Generation = 7, Population = 10.
--0-0-
-----0
----0-
-0-0-0
------
-0-0-0

Generation = 8, Population = 8.
-0----
--0-0-
-0---0
0---0-
------
0-----

Generation = 9, Population = 7.
0---0-
-0----
----0-
---0--
----0-
-0----

Generation = 10, Population = 12.
---0-0
--0---
-0---0
--0-0-
-0---0
0-0-0-

Generation = 11, Population = 7.
------
------
0-0---
-0---0
--0-0-
-0----

Generation = 12, Population = 9.
------
0-0---
-0-0-0
--0---
-0----
0---0-

Generation = 13, Population = 9.
0-0---
-0---0
------
-0---0
----0-
---0-0

Generation = 14, Population = 10.
-0-0-0
----0-
------
0-0---
-0---0
--0-0-

Generation = 15, Population = 10.
0---0-
-0----
0-0-0-
-0-0-0
--0---
------

Generation = 16, Population = 7.
---0-0
0-----
---0--
----0-
-----0
--0---

Generation = 17, Population = 10.
0-0---
-0---0
0-0---
------
--0---
-0-0-0

Generation = 18, Population = 13.
-0-0-0
0-0-0-
-0-0-0
0-----
-----0
0-0---

Generation = 19, Population = 11.
--0-0-
-----0
0-0-0-
---0--
--0-0-
---0-0

Generation = 20, Population = 9.
-0----
0---0-
---0--
0-0-0-
-0----
----0-

Generation = 21, Population = 10.
--0-0-
-----0
----0-
-0-0-0
------
-0-0-0

Generation = 22, Population = 8.
-0----
--0-0-
-0---0
0---0-
------
0-----

Generation = 23, Population = 7.
0---0-
-0----
----0-
---0--
----0-
-0----

Generation = 24, Population = 12.
---0-0
--0---
-0---0
--0-0-
-0---0
0-0-0-

Generation = 25, Population = 7.
------
------
0-0---
-0---0
--0-0-
-0----

*** Life<FredkinCell> 4x2 ***

Generation = 0, Population = 8.
00
00
00
00

Generation = 4, Population = 4.
--
22
22
--

Generation = 8, Population = 4.
22
--
--
22

Generation = 12, Population = 8.
44
44
44
44

Generation = 16, Population = 4.
--
66
66
--

Generation = 20, Population = 4.
66
--
--
66
